Title: La «Software Freedom Conservancy» necessita la vostra ajuda!
Slug: conservancy-fundraising-campaign
Date: 2015-12-04 00:30
Author: Mehdi Dogguy
Translator: Adrià García-Alzórriz, Innocent De Marchi, Lluís Gili
Lang: ca
Tags: sfc, conservancy, fundraising, contributing, donation
Status: published

[![Logo de la Software Freedom Conservancy](|filename|/images/conservancy-logo.png)](https://sfconservancy.org/)

«La *[Software Freedom Conservancy](https://sfconservancy.org/) contribueix
a promoure, millorar, desenvolupar i defensar projectes de programari
lliure i codi obert (FLOSS)*». Així és com es defineix a sí mateixa la 
«Software Freedom Conservancy». Organitzacions com la «Conservancy» permeten
els desenvolupadors de programari centrar-se en el que millor saben fer mentre
la «Conservancy» s'ocupa de fer complir el «copyleft», dels aspectes legals i
de proporcionar [diversos servicis](https://sfconservancy.org/members/services/).

El passat agost, Debian i la «Conservancy»
[van anunciar](http://sfconservancy.org/news/2015/aug/17/debian/) 
un acord de col·laboració i van formar el projecte d'agregació de
«copyright», pel que, entre d'altres coses, la «Conservancy» podrà ser
titular dels drets d'autor d'algunes obres de Debian i assegurar el
compliment del copyleft per tal que aquestes obres romanguin com a 
programari lliure.

Fa poc, la «Conservancy» va llançar una gran campanya de captació
de fons i necessita més donants individuals amb la finalitat de
finançar-se de la manera més sostenible i independent.
Això permetrà a la «Conservancy» continuar amb els seus esforços per
tal de convèncer més empreses a complir amb les llicències del programari
lliure com la GPL i a prendre accions legals quan el diàleg no funcioni.
La «Conservancy» necessita la vostra ajuda ara més que mai.

Molts desenvolupadors i col·laboradors de Debian ja s'han convertit
en [donants de la «Conservancy»](https://sfconservancy.org/sponsors#supporters). 

Si us plau, considereu registrar-vos com a donants a 
[https://sfconservancy.org/supporter/](https://sfconservancy.org/supporter/)!
