Title: DebConf17 organization started
Date: 2016-09-05 14:00
Tags: debconf, debconf17
Slug: debconf17-organization-started
Author: Laura Arjona Reina
Status: published

DebConf17 will take place in Montreal, Canada from August 6 to August 12, 2017.
It will be preceded by DebCamp, July 31 to August 4, and Debian Day, August 5.

We invite everyone to join us in organizing DebConf17.
There are different areas where your help could be very valuable,
and we are always looking forward to your ideas.

The DebConf content team is open to suggestions for invited speakers.
If you'd like to propose somebody who is not a regular DebConf attendee
follow the details in the [call for speaker proposals](http://blog.debconf.org/blog/debconf17/2016-08-22_call_for_speakers.dc) blog post.

We are also beginning to contact potential sponsors from all around the globe.
If you know any organization that could be interested,
please consider handing them
the [sponsorship brochure](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_brochure_en.pdf)
or [contact the fundraising team](mailto:sponsors@debconf.org) with any leads.

The DebConf team is holding IRC meetings every two weeks.
Have a look at the DebConf17 [website](https://debconf17.debconf.org) and [wiki page](https://wiki.debconf.org/wiki/DebConf17),
and engage in the IRC channels and the mailing list.

Let’s work together, as every year, on making the best DebConf ever!
