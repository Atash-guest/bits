Title: Tổ chức DebConf17 đã bắt đầu
Date: 2016-09-05 14:00
Tags: debconf, debconf17
Slug: debconf17-organization-started
Author: Laura Arjona Reina
Translator: Giap Tran
Lang: vi
Status: published

Debconf17 sẽ diễn ra tại Montreal, Canada từ ngày 6 đến ngày 12 tháng 8 năm 2017.
Sự kiện diễn ra với sự mở màn của Debcamp từ 31 tháng 7 tới 4 tháng 8, và Debian Day là 5 tháng 8.

Chúng tôi mời tất cả mọi người tham dự tổ chức Debiconf17.
Có nhiều lĩnh vực khác nhau mà bạn có thể giúp đỡ rất hữu ích,
và chúng tôi luôn mong chờ ý tưởng từ các bạn.

Nhóm phụ trách nội dung Deconf được mở ra nhằm mục đích gợi ý cho những người diễn thuyết.
Nếu bạn muốn đề xuất ai đó không phải là một người tham dự Debconf thường xuyên
thì hãy làm theo các chỉ dẫn trong bài viết [kêu gọi đề xuất người diễn thuyết](http://blog.debconf.org/blog/debconf17/2016-08-22_call_for_speakers.dc)

Chúng tôi cũng đang bắt đầu liên hệ với các nhà tài trợ tiềm năng từ khắp nơi trên thế giới.
Nếu bạn biết bất kỳ tổ chức nào có thể quan tâm,
xin vui lòng xem xét giới thiệu cho họ [các tài liệu tài trợ](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_brochure_en.pdf)
hoặc [liên hệ với nhóm gây quỹ](mailto:sponsors@debconf.org)

Nhóm Debconf được tổ chức họp IRC hai tuần 1 lần.
Được báo cáo lại tại DebConf17 [trang thông tin điện tử](https://debconf17.debconf.org) và [trang wiki](https://wiki.debconf.org/wiki/DebConf17), và các kênh IRC và bó thư chung.

Hãy làm việc với nhau, như mọi năm, và cùng đóng góp để Debconf trở nên tốt hơn bao giờ hết!
